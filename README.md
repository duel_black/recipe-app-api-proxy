# Recipe App API Proxy

NGINX proxy app for the Recipe App.

Recipe is used as a placedholder app to 
build an enterprise level dev ops solution.

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - The hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - The post of the app to foward requests to (default: `9000`)

### Commands
* Build Docker Image: `docker build -t proxy .` (Tagged proxy)