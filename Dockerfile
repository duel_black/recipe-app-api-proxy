# Use this base image because it doesn't run as a root user in production
FROM nginxinc/nginx-unprivileged:1-alpine

LABEL maintainer="Lucid Lumin"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

# Switch to root user so we can create directories in nginx on the docker image
USER root

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Switch back to nginx user now that directories are created and permissions are set
USER nginx

CMD ["/entrypoint.sh"]